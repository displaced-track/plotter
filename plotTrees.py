#!/usr/bin/env python
'''
plot.py is the main script to do the plotting
This reads the ntuples produced by SusySkimHiggsino
Makes plots of data vs MC in various variables
Configure various aspects in
  - cuts.py
  - samples.py
  - variables.py
One specifies the samples to be plotted at the top of calc_selections() function
'''
# So Root ignores command line inputs so we can use argparse
import time
from math import sqrt
# from random import gauss #seg fault?!
import os, time, argparse
from array import array

from samples_trees import configure_samples
from variables import *

from cuts import configure_cuts

import numpy as np
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(1)

from ROOT import THStack, TCanvas, TPad, kBlue, kGray, kBlack,kWhite, kOrange, kAzure, TChain, TLine, TH1D, TLegend, gPad, gStyle, TGaxis, TArrow, TLatex

#for correlation plots
doCorr = False

TreePath="/eos/atlas/atlascerngroupdisk/phys-susy/EWKDisplacedTrack_SUSY-2024-06/ntuples_v2/FlatNtuples/Run2"
# The path/folder name where the plots will be saved -- the folder will be created if it doesn't exist already
savedir = "Output"

if doCorr:
  TreePath = "/Correlations"

#___________________________________________________________
# Luminosity (for now only used as label in the plots)
#are these correct?
lumi_15_16 = 36.2 # [1/fb] 15+16
lumi_17 = 44.3 # [1/fb] 17
lumi_18 = 58.5 # [1/fb] 18

lumi_22 = 35.7 # [1/fb] 22

lumi_15_17 = lumi_15_16 + lumi_17 # [1/fb] 15+16+17
lumi_15_18 = lumi_15_16 + lumi_17 + lumi_18 # [1/fb] 15+16+17+18

    
#____________________________________________________________________________
def main():
  global data_period, isData15_16, isData17, isData18, isData15_17, isData15_18, savedir, lumi, TreePath, ATL_status, text_size, NTUP_status
  
  #default values
  var = "MET"
  region = "presel"
  unblind = False

  # check user has inputted variables or not
  parser = argparse.ArgumentParser(description='Analyse background/signal TTrees and make plots.')
  parser.add_argument('-v', '--variable',  type=str, nargs='?', help='String name for the variable (as appearing in the TTree) to make N-1 in.', default=var)
  parser.add_argument('-r', '--region',    type=str, nargs='?', help='String name of selection (signal/control) region to perform N-1 selection.', default=region)
  # parser.add_argument('-l', '--lumi',      type=str, nargs='?', help='Float of integrated luminosity to normalise MC to.', default=lumi)
  # parser.add_argument('-t', '--ttbarSamp', type=str, nargs='?', help='ttbar sample to use.', default=ttbarSamp)
  parser.add_argument('-u', '--unblind', action="store_true", help='Should the data be unblinded?')
  # parser.add_argument('-F', '--Force', action="store_true", help='Do you really want to unblind this SR?')
  # parser.add_argument('-p', '--period',  type=str, nargs='?', help='Set data period: data15-16, data17, data18 or data15-18.')
  # parser.add_argument('-L', '--label',  type=str, nargs='?', help='Append descriptive label to output filename.', default=save_label)
  # parser.add_argument('-a', '--cutArrow',  action='store_true', help='Draw arrows where cuts are placed for N-1 plots.')
  # parser.add_argument('-n', '--noLogY',  action='store_true', help='Do not draw log Y axis.')
  # parser.add_argument('-o', '--notShowOverflow',  action='store_true', help='Do not include overflow in bin N.')
  # parser.add_argument('-N', '--allNCuts',  action='store_true', help='Keep cut on variable to be plotted (if it exists).') # todo hacked
 
  args = parser.parse_args()
  if args.variable:
    var      = args.variable
  if args.region:
    region = args.region
  if args.unblind:
     unblind = args.unblind

  #CAREFUL not to unblind!
  if "SR" in region or "blind" in region: unblind = False
  if var == "BDTDeltaM100_90" or var == "BDTDeltaM100_90_high": unblind = False # e.g. at presel can accidently unblind
  if ("VR" in region or "CR" in region): unblind = True

  t0 = time.time()
  ATL_status = 'Internal'

  NTUP_status = 'SUSY-EWK-DT'
  # text size as percentage
  text_size = 0.045


   #================================================
  # make (relative) save directory if needed 
  if doCorr: savedir = os.path.join(TreePath,region)
  mkdir(savedir)

  #================================================
  # default values
  data_period = 'data15-18'
  save_label = 'non'
  isData15_16 = False
  isData17 = False
  isData18 = False
  isData15_17 = False
  isData15_18 = True
  lumi    = lumi_22
  cutArrow = False
  IsLogY = True
  showOverflow = True # hacked todo
  allNCuts = True # hacked todo


  print( '=========================================' )
  print( 'Data directory: {0}'.format(TreePath) )
  if isData15_16:
    print( 'MC directory: {0}'.format(TreePath) )
  elif isData17:
    print( 'MC directory: {0}'.format(TreePath) )
  elif isData18:
    print( 'MC directory: {0}'.format(TreePath) )
  elif isData15_18:
    print( 'MC directory: {0}'.format(TreePath) )
    print( 'MC directory: {0}'.format(TreePath) )
    print( 'MC directory: {0}'.format(TreePath) )
  print( 'Plotting variable: {0}'.format(var) )
  print( 'Selection region: {0}'.format(region) )
  print( 'Data-taking period: {0}'.format(data_period) )
  print( 'Normalising luminosity: {0}'.format(lumi) )
  #print( 'ttbar Sample: {0}'.format(ttbarSamp) )
  print( '=========================================\n' )
  

  save_var = var
  # convert maths characters are legit file names
  if '/' in var:
    save_var = var.replace('/', 'Over', 1)
  if '(' in var:
    save_var = save_var.replace('(', '', 1)
  if ')' in var:
    save_var = save_var.replace(')', '', 1)
  if IsLogY:
    save_name = savedir + '/hist1d_{0}_{1}'.format(save_var, region)
  if not IsLogY:
    save_name = savedir + '/hist1d_{0}_{1}_noLogY'.format(save_var, region)
  if save_label != 'non':
    save_name = save_name + '_' + save_label

  add_cut = "1"

  annotate_text = ''
  if isData15_16:
    annotate_text = '2015-16 data vs. mc16a'
  elif isData17:
    annotate_text = '2017 data vs. mc16d'
  elif isData18:
    annotate_text = '2018 data vs. mc16e'
  elif isData15_18:
    annotate_text = '2015-18 data vs. mc16a+d+e'
  

  #==========================================================
  # List samples to analyse 
  #==========================================================
    
  Force = False
  if unblind and not Force and region[:2] == "SR":
      print("You are trying to unblind a region whose name begins with \"SR\".")
      print("If you really really mean to, add the argument -F.")
      return
  if Force and region[:2] != "SR":
      print("You are trying to force unblinding without \"SR\".")
      print("This is dangerous. Remove -F.")
      return   

  calc_selections(var, add_cut, lumi, save_name, region, annotate_text, 'ttbar', unblind, cutArrow, IsLogY, showOverflow, allNCuts)

  tfinish = time.time()
  telapse = tfinish - t0
  print( '{0:.3f}s'.format(telapse))

#____________________________________________________________________________
def calc_selections(var, add_cuts, lumi, save_name, region, annotate_text='', ttbarSamp='ttbar', unblind=False, cutArrow=False, IsLogY=True, showOverflow=True, allNCuts=False):
  '''
  Extract trees given a relevant variable
  '''
  #==========================================================
  # Prepare information and objects for analysis and plots
  #==========================================================
  l_samp_bkg = ['diboson','singletop','Ztautau','Zmumu','Zee','Wenu','Wmunu','Wtaunu','Znunu','ttbar','dijet']

  #######################
  #signals here...
  
  l_samp_signal = ["176_175p5_175"]
  l_samp_other = l_samp_signal
  # blind the SRs
  if unblind:
    l_samp_other += ['data']
  
  l_samp_all =   l_samp_bkg + l_samp_other


  # obtain cut to apply (string)
  normCutsAfter = configure_cuts(var, add_cuts, region, isData18, allNCuts) 
  
  # get dictionary defining sample properties
  d_samp = configure_samples(isData15_16, isData17, isData18, isData15_18, False)
  
  # get dictionary of histogram configurations
  d_vars = configure_vars(region)
  # obtain the number of bins with their xmin and xmax
  hNbins = d_vars[var]['hXNbins']
  hXmin  = d_vars[var]['hXmin']
  hXmax  = d_vars[var]['hXmax']

  variable_bin = False
  hXarray = []
  if hNbins == 'var':
    variable_bin = True
    hXarray = d_vars[var]['binsLowE']  
    hNbins = len(hXarray) - 1

  # declare stacked background  
  hs = THStack('','')
  hs_intgl_low = THStack('','') # lower cut integral (for significance cut)
  hs_intgl_upp = THStack('','') # upper cut integral (for significance cut)
  #hs_intgl_overflow = THStack('','') # overflow included in the bin N
 
  # initialise objects to fill in loop 
  d_files = {}
  d_hists = {}
  d_yield = {}
  d_yieldErr = {}
  #d_yield_overflow = {}
  nTotBkg = 0 # yield of background
  nVarBkg = 0 # variance of background
  #nTotBkg_overflow = 0 # yield of background with overflow included in bin N
  #nVarBkg_overflow = 0 # variance of background with overflow included in bin N
 
  l_bkg = []
  l_sig = []
  
  h_dat = 0
  N_dat = 0
  #N_dat_overflow = 0

  #==========================================================
  # loop through samples, fill histograms
  #==========================================================
  l_styles = [1, 2, 3, 4]

  Nsignal_count = 0
  #Nsignal_overflow_count = 0
  for samp in l_samp_all:
    if 'data' in samp and not unblind:
      continue
    #print( 'Processing {0}'.format(samp) )
    # obtain sample attributes 


    sample_type = d_samp[samp]['type']
    path        = d_samp[samp]['path']
  
    # Choose full path of sample by its type  
    full_path_data15_16 = ''
    full_path_data17 = ''
    full_path_data18 = ''
    if sample_type == 'sig':
        l_color     = d_samp[samp]['l_color']
        full_path_data15_16 = TreePath + '/' + path
        full_path_data17    = TreePath + '/' + path
        full_path_data18    = TreePath + '/' + path
        l_sig.append(samp)
    elif sample_type == 'bkg':
        f_color     = d_samp[samp]['f_color']
        full_path_data15_16 = TreePath + '/' + path
        full_path_data17    = TreePath + '/' + path
        full_path_data18    = TreePath + '/' + path
        l_bkg.append(samp)
    elif sample_type == 'data':
        full_path_data15_16 = TreePath + '/' + path
        full_path_data17 = TreePath + '/' + path
        full_path_data18 = TreePath + '/' + path
    
    cutsAfter = normCutsAfter 
    # assign list of TFiles for each data period (mc campaign) to a dictionary entry
    d_files[samp] = [full_path_data15_16, full_path_data17, full_path_data18]

    # obtain histogram from file and store to dictionary entry
    d_hists[samp] = tree_get_th1f( d_files[samp], samp, var, cutsAfter, hNbins, hXmin, hXmax, lumi, variable_bin, hXarray, showOverflow)

    # ---------------------------------------------------- 
    # Stacked histogram: construct and format
    # ---------------------------------------------------- 
    # extract key outputs of histogram 
    hist        = d_hists[samp][0]
    nYield      = d_hists[samp][1]
    h_intgl_low = d_hists[samp][2]
    h_intgl_upp = d_hists[samp][3]
    nYieldErr   = d_hists[samp][4]
    #h_intgl_overflow = d_hists[samp][5]
    #nYield_overflow  = d_hists[samp][6]
    #nYieldErr_overflow   = d_hists[samp][7]
    
    # samp : nYield number of tracks for each sample
    d_yield[samp]    = nYield
    d_yieldErr[samp] = nYieldErr
    #d_yield_overflow[samp]    = nYield_overflow

    # add background to stacked histograms
    if sample_type == 'bkg':
      hs.Add(hist)
      hs_intgl_low.Add(h_intgl_low)
      hs_intgl_upp.Add(h_intgl_upp)
      #hs_intgl_overflow.Add(h_intgl_overflow)
      
      format_hist(hist, 1, 0, 1, f_color, 1001, 0)
      nTotBkg  += nYield
      nVarBkg  += nYieldErr ** 2
      #nTotBkg_overflow  += nYield_overflow
      #nVarBkg_overflow  += nYieldErr_overflow ** 2
    
    if sample_type == 'sig':
        format_hist(hist, 3, l_color, l_styles[Nsignal_count], f_color=0)
      #Nsignal_count += 1
    
    if sample_type == 'data':
      h_dat = hist
      format_hist(h_dat,  1, kBlack, 1)
      N_dat = nYield
      #N_dat_overflow = nYield_overflow
      
  errStatBkg = sqrt( nTotBkg )
  errTotBkg  = sqrt( errStatBkg**2 + (0.2 * nTotBkg) ** 2 ) # total error as sum in quadrature of stat errors and 20% flat syst
  
  print('errStatBkg (sqrtBkg): {0:.3f}, errTotBkg (stat + 20% flat syst): {1:.3f}'.format(errStatBkg, errTotBkg))

  print('==============================================')
  print('{0}, Data, {1}'.format(region, N_dat))
  print('----------------------------------------------')
  print('{0}, Total bkg, {1:.3f} +/- {2:.3f}'.format(region, nTotBkg, errStatBkg))
  print('----------------------------------------------')

  # legend for signals, data and total bkg yield
  leg = mk_leg(0.44, 0.68, 0.55, 0.93, region, l_samp_other, d_samp, nTotBkg, d_hists, d_yield, d_yieldErr, sampSet_type='bkg', txt_size=0.03)
  # then, legend with breakdown of background by sample
  d_bkg_leg = {}
  l_bkg_leg = ['samp_bkg']
  d_bkg_leg['samp_bkg'] = mk_leg(0.70, 0.65, 0.86, 0.88, region, l_samp_bkg, d_samp, nTotBkg, d_hists, d_yield, d_yieldErr, sampSet_type='bkg', txt_size=0.03)
  print('==============================================')
  
  #============================================================
  # make MC error histogram (background uncertainty hatching)
  pc_sys = 0 # percentage systematic uncertainty
  h_mcErr = mk_mcErr(hs, pc_sys, hNbins, hXmin, hXmax, variable_bin, hXarray)
  h_mcErr.SetFillStyle(3245) # hatching 
  h_mcErr.SetFillColor(kGray+2)
  h_mcErr.SetLineWidth(2)
  # make other markings invisible
  #h_mcErr.SetLineColorAlpha(kWhite, 0)
  h_mcErr.SetLineColorAlpha(kGray+2, 1.0)
  h_mcErr.SetMarkerColorAlpha(kWhite, 0)
  h_mcErr.SetMarkerSize(0)
  #h_mcErr.SetMarkerColorAlpha(kWhite, 0)
  if 'Pass' in region or 'presel' in region:
    leg.AddEntry(h_mcErr, 'SM ({0:.1f} Tracks)'.format(nTotBkg), 'lf')
    #leg.AddEntry(h_mcErr, 'SM #oplus 20% syst ({0:.1f})'.format(nTotBkg), 'lf')
  else:
    #leg.AddEntry(h_mcErr, '#scale[0.8]{SM #oplus 20% syst} ' + '({0:.1f} #pm {1:.1f})'.format(nTotBkg, errTotBkg), 'lf')
    leg.AddEntry(h_mcErr, '#scale[0.8]{SM} Tracks' + '({0:.1f})'.format(nTotBkg), 'lf')
  
  #leg.AddEntry(h_mcErr, 'SM 20% syst ({0:.1f})'.format(nTotBkg), 'lf')
  
  #============================================================
  # Now all background histogram and signals obtained
  # Proceed to make significance scan
  #============================================================

  # dicitonary for histograms and its significance plots
  # in format {samp_name : histogram}
  d_hsig = {}
  d_hsigZ20 = {}
  d_hsigZ30 = {}
  #d_hsig_overflow = {}
  
  # obtain direction of cut  
  cut_dir = d_vars[var]['cut_dir'] 
  
  # calculate significances for signals only
  for samp in l_samp_signal:

    sample_type = d_samp[samp]['type']
    if sample_type == 'sig':
      print("Making Z for sample = {}".format(samp))
      d_hsig[samp] = d_hists[samp][0]
      h_signal_low = d_hists[samp][2]
      h_signal_upp = d_hists[samp][3]
      #d_hsig_overflow[samp] = d_hists[samp][5]
      if True: # True for cumulated Z, False to puntual Z.
          # significance based on cutting to left (veto right)
          if 'left' in cut_dir:
              d_hsigZ20[samp] = mk_sigZ_plot(h_signal_low, hs_intgl_low, 20, hNbins, hXmin, hXmax, variable_bin, hXarray,histoName=samp)
          # significance based on cutting to right (veto left)
          if 'right' in cut_dir:
              d_hsigZ20[samp] = mk_sigZ_plot(h_signal_upp, hs_intgl_upp, 20, hNbins, hXmin, hXmax, variable_bin, hXarray,histoName=samp)
      else: #significance puntual for each bin
          d_hsigZ20[samp] = mk_sigZ_plot(d_hsig[samp], hs, 20, hNbins, hXmin, hXmax, variable_bin, hXarray,histoName=samp) 
  '''
  if 'SR' not in region:  
    # ensure to manually set the bin error so ratio plot is correct
    for mybin in range( h_dat.GetXaxis().GetNbins() + 1 ):  
      yval = h_dat.GetBinContent(mybin)
      yerr = h_dat.GetBinError(mybin)
      h_dat.SetBinError(mybin, yerr)
  '''
  #============================================================
  # proceed to plot
  #if showOverflow:
  #  plot_selections(var, hs_overflow, d_hsig_overflow, h_dat_overflow, h_mcErr, d_hsigZ30, d_hsigZ20, leg, l_bkg_leg, d_bkg_leg, lumi, save_name, pc_sys, region, nTotBkg, l_sig, cutsAfter, annotate_text, variable_bin, unblind, cutArrow, IsLogY)
  #else:
  #plot_selections(var, hs, d_hsig, h_dat, h_mcErr, d_hsigZ30, d_hsigZ20, leg, l_bkg_leg, d_bkg_leg, lumi, save_name, pc_sys, region, nTotBkg, l_sig, cutsAfter, annotate_text, variable_bin, unblind, cutArrow, IsLogY)
  # plot log Y only for now
  plot_selections(var, hs, d_hsig, h_dat, h_mcErr, d_hsigZ20, d_hsigZ20, leg, l_bkg_leg, d_bkg_leg, lumi, save_name + "logY", pc_sys, region, nTotBkg, l_sig, cutsAfter, annotate_text, variable_bin, unblind, cutArrow, IsLogY=True)
  # plot linear Y
  plot_selections(var, hs, d_hsig, h_dat, h_mcErr, d_hsigZ20, d_hsigZ20, leg, l_bkg_leg, d_bkg_leg, lumi, save_name, pc_sys, region, nTotBkg, l_sig, cutsAfter, annotate_text, variable_bin, unblind, cutArrow, IsLogY=False)
  
  return nTotBkg

#____________________________________________________________________________
def plot_selections(var, h_bkg, d_hsig, h_dat, h_mcErr, d_hsigZ20, d_hsigZ30, leg, l_bkg_leg, d_bkg_leg, lumi, save_name, pc_sys, region, nTotBkg, l_sig, cutsAfter, annotate_text, variable_bin, unblind=False, cutArrow=False, IsLogY=True):
  '''
  plots the variable var given input THStack h_bkg, one signal histogram and legend built
  makes a dat / bkg panel in lower part of figure
  to-do: should be able to read in a list of signals
  '''
  print('Proceeding to plot')
  
  

  # gPad left/right margins
  gpLeft = 0.17
  gpRight = 0.05
  
  
  d_vars = configure_vars(region)
  
  #==========================================================
  # build canvas
  can  = TCanvas('','',1000,1000)
  customise_gPad()
  
  pad1 = TPad('pad1', '', 0.0, 0.40, 1.0, 1.0)
  pad2 = TPad('pad2', '', 0.0, 0.00, 1.0, 0.4)
  if not unblind:
      pad2.SetLogy()
  pad1.Draw()
  pad1.cd()
  
  if IsLogY:
    pad1.SetLogy()
  customise_gPad(top=0.03, bot=0.02, left=gpLeft, right=gpRight)

  #=============================================================
  # draw and decorate
  # draw elements
  h_bkg.Draw('hist')
  # draw signal samples
  for samp in d_hsig:
    print('Drawing {0}'.format(samp))
    d_hsig[samp].Draw('hist same') #e2 = error coloured band
  # clone the total background histogram to draw the line
  h_mcErr_clone = h_mcErr.Clone()
  h_mcErr_clone.SetFillColorAlpha(kWhite, 0)
  h_mcErr_clone.SetFillStyle(0)
  h_mcErr.Draw('same e2')
  h_mcErr_clone.Draw('same hist')
  
  # IMPORTANT: blind data
  if unblind:
    #pass
    h_dat.Draw('hist same ep')

    # Data point size 
    h_dat.SetMarkerSize(1.9)
    h_dat.SetLineWidth(2)
  
  leg.Draw('same')
  for bkg_leg in l_bkg_leg:
    d_bkg_leg[bkg_leg].Draw('same')
 
  ''' 
  # put a white outline around the data points
  h_dat_outline = h_dat.Clone()
  h_dat_outline.SetMarkerStyle(25) # 25 = open square
  h_dat_outline.SetMarkerColor(kWhite)
  h_dat_outline.Draw('hist same ep')
  '''
  
  #==========================================================
  # calculate bin width 
  hNbins = d_vars[var]['hXNbins']
  hXmin  = d_vars[var]['hXmin']
  hXmax  = d_vars[var]['hXmax']
  if not variable_bin:
    binWidth = (hXmax - hXmin) / float(hNbins)
  
  # label axes of top pad
  xtitle = ''
  binUnits = d_vars[var]['units']
  if variable_bin:
    ytitle = 'Tracks / bin'
  elif 0.1 < binWidth < 1:
    #ytitle = 'Tracks / {0:.2f} {1}'.format(binWidth, binUnits)
    ytitle = 'Tracks / {0} {1}'.format(binWidth, binUnits)
  elif binWidth <= 0.1:
    #ytitle = 'Tracks / {0:.2f} {1}'.format(binWidth, binUnits)
    ytitle = 'Tracks / {0} {1}'.format(binWidth, binUnits)
  elif binWidth >= 1:
    #ytitle = 'Tracks / {0:.0f} {1}'.format(binWidth, binUnits)
    ytitle = 'Tracks / {0} {1}'.format(binWidth, binUnits)
    
  enlargeYaxis = False
  if 'Pass' in region or 'preselect' in region:
    enlargeYaxis = True
  
  customise_axes(h_bkg, xtitle, ytitle, IsLogY, enlargeYaxis, d_hsig=d_hsig, h_dat=h_dat)
  
  #==========================================================
  # arrow to indicate where cut is
  # Case 2-sided cuts
  
  # set height of arrow
  ymin_Ar = gPad.GetUymin()
  ymax_Ar = h_bkg.GetMaximum()
  if IsLogY:
    ymax_Ar = 80
  if not IsLogY:
    ymax_Ar = 0.8*ymax_Ar
  # arrow width is 5% of the maximum x-axis bin 
  arr_width = hXmax * 0.06
  if 'cut_pos2' in d_vars[var].keys():
    cut_pos2 = d_vars[var]['cut_pos2']
    cut_dir2 = d_vars[var]['cut_dir2']
    cutAr2   = cut_arrow(cut_pos2, ymin_Ar, cut_pos2, ymax_Ar, cut_dir2, 0.012, arr_width)
    if cutArrow:
      cutAr2[0].Draw()
      cutAr2[1].Draw()
  # otherwise 1-sided cut 
  cut_pos = d_vars[var]['cut_pos']
  cut_dir = d_vars[var]['cut_dir']
  cutAr = cut_arrow(cut_pos, ymin_Ar, cut_pos, ymax_Ar, cut_dir, 0.012, arr_width)
  if cutArrow:
    cutAr[0].Draw()
    cutAr[1].Draw()

  # replace -mm with mu mu
  if '-ee' in region:
    region = region.replace('-ee', ' ee', 1)
  if '-mm' in region:
    region = region.replace('-mm', ' #mu#mu', 1)
  if '-em' in region:
    region = region.replace('-em', ' e#mu', 1)
  if '-me' in region:
    region = region.replace('-me', ' #mue', 1)
  if '-ee-me' in region:
    region = region.replace('-ee-me', ' ee+#mue', 1)
  if '-mm-em' in region:
    region = region.replace('-mm-em', ' #mu#mu+e#mu', 1)

  if '-SF' in region:
    region = region.replace('-SF', ' ee+#mu#mu', 1)
  if '-DF' in region:
    region = region.replace('-DF', ' e#mu+#mue', 1)
  if '-AF' in region:
    region = region.replace('-AF', ' ee+#mu#mu+e#mu+#mue', 1)
  
  #==========================================================
  # Text for ATLAS, energy, lumi, region, ntuple status
  myText(0.2, 0.87, '#bf{#it{ATLAS}} ' + ATL_status, text_size*1.2, kBlack)
  #myText(0.2, 0.81, '13.6 TeV, {0:.1f}'.format(float(lumi)) + ' fb^{#minus1}', text_size*1.1, kBlack)
  if 'Run3' in TreePath: 
      isRun3 = True
  elif 'Run2' in TreePath: 
      isRun3 = False
            
  if isRun3==True:
      myText(0.2, 0.77, "Run3 - 2022", text_size*0.7, kBlack) 
      myText(0.2, 0.81, '13.6 TeV, {0:.1f}'.format(float(lumi_22)) + ' fb^{#minus1}', text_size*1.1, kBlack) 
  else:
      myText(0.2, 0.77, "Run2 2015-2018", text_size*0.7, kBlack) 
      myText(0.2, 0.81, '13 TeV, {0:.1f}'.format(float(lumi_15_18)) + ' fb^{#minus1}', text_size*1.1, kBlack) 
  myText(0.2, 0.73, NTUP_status, text_size*0.7, kBlack) 
  myText(0.2, 0.69, region, text_size*0.7, kBlack) 
  
  #if not annotate_text == '':
  #  myText(0.2, 0.69, annotate_text, text_size*0.7, kGray+1) 
  
  gPad.RedrawAxis() 
  
  #==========================================================
  # go to pad 2: significance panel
  #==========================================================
  can.cd()
  pad2.Draw()
  pad2.cd()
  customise_gPad(top=0.05, bot=0.39, left=gpLeft, right=gpRight)
  
  varTeX = 'tlatex'
  
  Xunits = d_vars[var]['units']
  if Xunits == '':
    xtitle = '{0}'.format( d_vars[var]['tlatex'])
  else:
    xtitle = '{0} [{1}]'.format( d_vars[var][varTeX], Xunits ) 

  # unblinded ratio against data
  if unblind:
    #==========================================================
    # MC error ratio with MC
    h_mcErrRatio = h_mcErr.Clone()
    h_mcErrRatio.Divide(h_bkg.GetStack().Last())
    h_mcErrRatio.SetFillStyle(3245)
    h_mcErrRatio.SetFillColor(kGray+2)
    h_mcErrRatio.SetMarkerSize(0) 
    h_mcErrRatio.Draw('e2')
    
    # draw line for the ratios
    l = draw_line(hXmin, 1, hXmax, 1, color=kGray+2, style=1) 
    l.Draw()
    
    # draw data on top
    hRatio = h_dat.Clone()
    hRatio.Divide(h_bkg.GetStack().Last())  
    hRatio.Draw('same ep') 
    
    # ensure uncertainties in ratio panel are consistent with upper plot
    for ibin in range(0, hRatio.GetNbinsX()+1) :
      ratioContent = hRatio.GetBinContent(ibin)
      dataError = h_dat.GetBinError(ibin)
      dataContent = h_dat.GetBinContent(ibin)
      if ratioContent > 0 :
        hRatio.SetBinError(ibin, ratioContent * dataError / dataContent) 
    
    # code to draw error bars even if point outside range 
    #hRatio.Draw("PE same")
    oldSize = hRatio.GetMarkerSize()
    hRatio.SetMarkerSize(0)
    hRatio.DrawCopy("same e0")
    hRatio.SetMarkerSize(oldSize)
    hRatio.Draw("PE same") 
    hRatio.GetYaxis().SetTickSize(0)
  
    ytitle = 'Data / SM'
    customise_axes(h_mcErrRatio, xtitle, ytitle)
    gPad.RedrawAxis() 
    
    # insert arrows indicating data point is out of range of ratio panel
    l_arrows = {} 
    for mybin in range( hRatio.GetXaxis().GetNbins()+1 ):  
      Rdat = hRatio.GetBinContent(mybin)
      xval = hRatio.GetBinCenter(mybin)
      #print( 'Rdat: {0}, xval: {1}'.format(Rdat, xval) )
      if Rdat > 2:
        l_arrows[xval] = cut_arrow( xval, 1.7, xval, 1.9, 'up', 0.008, 6, kOrange+2 )
        l_arrows[xval][1].Draw()
    #draw_data_vs_mc(h_dat, h_bkg, h_mcErr, xtitle, hXmin, hXmax) 
    #==========================================================
  # significance scans iff blinded
  else:
    draw_sig_scan(l_sig, d_hsigZ20, cut_dir, xtitle, hXmin, hXmax) 
    gPad.RedrawAxis() 
    
  # A simple S/B
  # for samp in d_hsig:
  #   h_SOverB = d_hsig[samp].Clone()
  #   h_SOverB.Divide(h_bkg.GetStack().Last())
  #   h_SOverB.SetFillStyle(3245)
  #   h_SOverB.SetFillColor(kGray+2)
  #   h_SOverB.SetMarkerSize(0) 
  #   h_SOverB.Draw()
  #   ytitle = 'S / B'
  #   customise_axes(h_SOverB, xtitle, ytitle)
  #   gPad.RedrawAxis() 
  
  #==========================================================
  # save everything
  can.cd()
  can.SaveAs(save_name + '.pdf')
  #can.SaveAs(save_name + '.eps')
  #can.SaveAs(save_name + '.png')
  can.Close()

  
#____________________________________________________________________________
def draw_sig_scan(l_signals, d_hsigZ, cut_dir, xtitle, hXmin, hXmax):
  '''
  Draw significance scan 
  for signals in list l_signals
  using significance histograms d_hsigZ
  labelled by cut_dir, xtitle
  in range hXmin, hXmax
  '''
  print('Making significance scan plot in lower panel')
  #----------------------------------------------------
  # draw significances
  d_samp = configure_samples(isData15_16, isData17, isData18, isData15_18, False)
  ytitle = 'Significance Z'


  for i, samp in enumerate(l_signals):
    hsigZ = d_hsigZ[samp]
    hsigZ.Draw('hist same')
    if i < 1:
      customise_axes(hsigZ, xtitle, ytitle, 1.2)
    
    l_color     = d_samp[samp]['l_color']
    format_hist(hsigZ, 2, l_color, 1, 0)



  # draw line for the ratio = 1
  l = draw_line(hXmin, 1.97, hXmax, 1.97, color=kAzure+1, style=7) 
  l.Draw()
  if 'left' in cut_dir:
    myText(0.50, 0.83, 'Cut left, Z(s #geq 3, b #geq 1, #Delta b/b = 20%)',  0.07, kBlack)
  if 'right' in cut_dir:
    myText(0.50, 0.83, 'Cut right, Z(s #geq 3, b #geq 1, #Delta b/b = 20%)', 0.07, kBlack)

#____________________________________________________________________________
def mk_sigZ_plot(h_intgl_sig, h_intgl_bkg, pc_syst, Nbins=100, xmin=0, xmax=100,variable_bin=False,hXarray=None,histoName=''):
  '''
  Takes background & signal one-sided integral histograms
  and input percentage systematic
  Returns the signal significance Z histogram
  '''
  print('Making significance plot')
  if variable_bin:     
    h_pcsyst = TH1D("Z_"+histoName, '', Nbins, array('d', hXarray) )
  else:
    h_pcsyst = TH1D("Z_"+histoName, '', Nbins, xmin, xmax)
    
  for my_bin in range( h_intgl_bkg.GetStack().Last().GetSize() ): 
    sExp     = h_intgl_sig.GetBinContent(my_bin)
    bExp     = h_intgl_bkg.GetStack().Last().GetBinContent(my_bin)  
    bin_low  = h_intgl_bkg.GetStack().Last().GetBinLowEdge( my_bin )
    # Case pathology 
    # Set significance is 0 if bExp or sExp is below 0
    #if bExp <= 0 or sExp <= 0:
    if bExp < 1 or sExp < 3:
      RS_sigZ = 0
    else:
      # add statistical and systematic uncertainties in quadrature
      #BUnc   = sqrt ( abs( bExp + ( ( pc_syst / float(100) ) * bExp ) ** 2 ) )
      #RS_sigZ = RooStats.NumberCountingUtils.BinomialExpZ( sExp, bExp, BUnc/float(bExp) )

      # Only give (relative) systematic uncertainty as third argument to the Z_N function
      #RS_sigZ = RooStats.NumberCountingUtils.BinomialExpZ( sExp, bExp, pc_syst / float(100) )
      RS_sigZ = ZValue(sExp,bExp,pc_syst / float(100))
      #h_pcsyst.Fill(bin_low, RS_sigZ)
      #print('{0}, {1}, {2}, {3}, {4}, {5}'.format(my_bin, bin_low, bExp, sExp, my_sigZ, RS_sigZ) )
      #BUnc05 = sqrt ( abs( bExp + ( 0.05 * bExp ) ** 2 ) )
      #BUnc20 = sqrt ( abs( bExp + ( 0.20 * bExp ) ** 2 ) )
      # calculate my significance
      #my_sigZ = sExp / float( BUnc )
      h_pcsyst.SetBinContent(my_bin, RS_sigZ)
   
  return h_pcsyst
    
#____________________________________________________________________________
def mk_mcErr(hStack, pc_sys, Nbins=100, xmin=0, xmax=100, variable_bin=False, hXarray=0):
  '''
  smear stacked MC histogram with 'Gaussian' sqrt(N) to emulate stats 
  also add a pc systematic
  '''
  if variable_bin:
    h_mcErr = TH1D('', "", Nbins, array('d', hXarray) )
  else:
    h_mcErr = TH1D('', "", Nbins, xmin, xmax)
  
  print( 'Making MC err' )
  for my_bin in range( hStack.GetStack().Last().GetSize() ):
    yval = hStack.GetStack().Last().GetBinContent(my_bin)
    
    if yval == 0:
      yval = 0.001
    # ============================================================ 
    # SERIOUS ISSUE: NEED TO INVESTIGATE!
    # why are there negative histogram values? something flawed going on
    # for now, take mean of adjacent bin y-values to disguise anomaly
    if yval < 0:
      yval = 0.001
      print( '\nSERIOUS WARNING: negative histogram value {0} in bin {1}'.format(yval, my_bin) )
      print( 'Please investigate. For now setting value to mean of neighbouring bins.\n' )
      #yMinus1 = hStack.GetStack().Last().GetBinContent(my_bin - 1)
      #yPlus1  = hStack.GetStack().Last().GetBinContent(my_bin + 1)
      #yval = (yPlus1 + yMinus1) / float(2)
    # ============================================================ 
  
    # get statistical variance as sum of weights squared
    yval_GetErr   = hStack.GetStack().Last().GetBinError(my_bin)
    # add stat and sys err in quadrature
    yval_err = sqrt( yval_GetErr ** 2 + ( 0.01 * pc_sys * yval ) ** 2 )
    h_mcErr.SetBinContent( my_bin, yval )
    h_mcErr.SetBinError(   my_bin, yval_err ) 
  
  return h_mcErr
   
#_______________________________________________________
def tree_get_th1f(f, hname, var, cutsAfter='', Nbins=100, xmin=0, xmax=100, lumifb=35, variable_bin=False, hXarray=0, showOverflow=True):
  '''
  from a TTree, project a leaf 'var' and return a TH1F
  '''
  print(hname)
  if variable_bin:
    h_AfterCut   = TH1D(hname + '_hist', "", Nbins, array('d', hXarray) )
    #hOneBin      = TH1D(hname + '_onebin', '', 2, 0, 2)
  else:
    h_AfterCut   = TH1D(hname + '_hist', "", Nbins, xmin, xmax)
    #hOneBin      = TH1D(hname + '_onebin', '', 2, 0, 2)
 
  h_AfterCut.Sumw2()
  
  lumi = "1." #ipb
  weights = "eventWeight*genWeight*pileupWeight*leptonWeight*JVTWeight*140000"
  
  if doCorr:
    lumi = "1."
    weights = "EventWeight"

  if hname == "FNP" or hname == "FNP_NONE":
    lumi = "1."
    weights = "FNPweight"

  cut_after = '({0}) * {1} * ({2})'.format(cutsAfter, weights, lumi) 

  print(cut_after)
  # ========================================================= 

  # Add TTrees from different periods to a TChain, 
  # and then fill histogram using TTree::Project() on the TChain
  print("hname=",hname)
  if ('Znunu' in hname) or ('Zee' in hname) or ('Zmumu' in hname) or ('Ztautau' in hname):
      chain = TChain( 'Zjets_NoSys' )
  elif ('Wenu' in hname) or ('Wmunu' in hname) or ('Wtaunu' in hname):
      chain = TChain( 'Wlnjets_NoSys' )
  elif 'data' in hname:
      chain = TChain( 'data22' )
  elif '176_175p5_175' in hname:
      chain = TChain( 'Higgsino_NoSys' )
  else:
      chain = TChain( hname + '_NoSys' )
      #chain = TChain( hname )
      
  #if using separate data?
  if isData15_16 or isData15_18:
    #print("hname = {}".format(hname))
    print("f[0] = {}".format(f[0]))
    chain.Add( f[0] )

  #if isData17 or isData15_18:
  #  chain.Add( f[1] )

  #if isData18 or isData15_18:
  #  chain.Add( f[2] )
  
  if 'data' not in hname:
    chain.Project( hname + '_hist', var, cut_after )
    #chain.Project( hname + '_onebin', 'lepSignal[0]', cut_after )
  elif 'data' in hname:
    chain.Project( hname + '_hist', var, cutsAfter )
    #chain.Project( hname + '_onebin', 'lepSignal[0]', cutsAfter )

  # =========================================================
  # perform integrals to find 
  # total yield, one-sided lower and upper cumulative histos
  import ctypes
  nYieldErr = 0
  nYield    = h_AfterCut.IntegralAndError(0, Nbins+1, ctypes.c_double(nYieldErr))
  nYieldErr = sqrt(nYield)
  
  h_intgl_lower = TH1D(hname + '_intgl_lower', "", Nbins, xmin, xmax)
  h_intgl_upper = TH1D(hname + '_intgl_upper', "", Nbins, xmin, xmax)

  #h_intgl_overflow = TH1D(hname + '_intgl_overflow', "", Nbins, xmin, xmax)
  #h_intgl_overflow.Sumw2()
  
  for my_bin in range( h_AfterCut.GetXaxis().GetNbins() + 1 ):
    
    # get lower edge of bin
    bin_low = h_AfterCut.GetXaxis().GetBinLowEdge( my_bin )
    
    # set the negatively weighted values to 0.
    bin_val = h_AfterCut.GetBinContent( my_bin )
    if bin_val < 0:
      print( 'WARNING: Bin {0} of sample {1} has negative entry, setting central value to 0.'.format(my_bin, hname) )
      h_AfterCut.SetBinContent(my_bin, 0.)
    
    # do one-sided integral either side of bin
    intgl_lower = h_AfterCut.Integral( 0, my_bin ) 
    intgl_upper = h_AfterCut.Integral( my_bin, Nbins+1 ) 
    h_intgl_lower.SetBinContent( my_bin, intgl_lower )
    h_intgl_upper.SetBinContent( my_bin, intgl_upper )

  h_intgl_overflow = h_AfterCut.Clone("h_intgl_overflow")
  content_binN = h_intgl_overflow.GetBinContent(Nbins)
  content_overflowBin = h_intgl_overflow.GetBinContent(Nbins+1)
  h_intgl_overflow.SetBinContent(Nbins, content_binN + content_overflowBin)
  h_intgl_overflow.SetBinContent(Nbins+1, 0.)
  nYieldErr_overflow = 0
  nYield_overflow = h_intgl_overflow.IntegralAndError(0, Nbins+1, ctypes.c_double(nYieldErr_overflow))

  print( 'Sample {0} has integral {1:.3f} +/- {2:.3f}'.format( hname, nYield, nYieldErr ) )

  # =========================================================

  if showOverflow:
    h_AfterCut = h_intgl_overflow
  
  return [h_AfterCut, nYield, h_intgl_lower, h_intgl_upper, nYieldErr, h_intgl_overflow, nYield_overflow]

#____________________________________________________________________________
def format_hist(hist, l_width=2, l_color=kBlue+2, l_style=1, f_color=0, f_style=1001, l_alpha=1.0):
  
  # lines
  hist.SetLineColorAlpha(l_color, l_alpha)
  hist.SetLineStyle(l_style)
  hist.SetLineWidth(l_width)
  
  # fills
  hist.SetFillColor(f_color)
  hist.SetFillStyle(f_style)
  # markers
  hist.SetMarkerColor(l_color)
  hist.SetMarkerSize(1.1)
  hist.SetMarkerStyle(20)


#____________________________________________________________________________
def customise_gPad(top=0.03, bot=0.15, left=0.17, right=0.08):
  gPad.Update()
  gStyle.SetTitleFontSize(0.0)
  
  # gPad margins
  gPad.SetTopMargin(top)
  gPad.SetBottomMargin(bot)
  gPad.SetLeftMargin(left)
  gPad.SetRightMargin(right)

  gStyle.SetOptStat(0) # hide usual stats box 
  
  gPad.Update()
  
#____________________________________________________________________________
def customise_axes(hist, xtitle, ytitle, IsLogY=False, enlargeYaxis=False, d_hsig=None, h_dat=None):
  # set a universal text size
  text_size = 45
  TGaxis.SetMaxDigits(4) 
  ##################################
  # X axis
  xax = hist.GetXaxis()
  
  # precision 3 Helvetica (specify label size in pixels)
  xax.SetLabelFont(43)
  xax.SetTitleFont(43)
  #xax.SetTitleFont(13) # times
  
  xax.SetTitle(xtitle)
  xax.SetTitleSize(text_size)
  # top panel
  if 'Tracks' in ytitle:
    xax.SetLabelSize(0)
    xax.SetLabelOffset(0.02)
    xax.SetTitleOffset(2.0)
    xax.SetTickSize(0.04)  
  # bottom panel
  else:
    xax.SetLabelSize(text_size - 7)
    xax.SetLabelOffset(0.03)
    xax.SetTitleOffset(1.5)
    xax.SetTickSize(0.08)
    
    xax.SetTitleSize(text_size/1.1)
     
  #xax.SetRangeUser(0,2000) 
  #xax.SetNdivisions(-505) 
  gPad.SetTickx() 
  
  ##################################
  # Y axis
  yax = hist.GetYaxis()
  # precision 3 Helvetica (specify label size in pixels)
  yax.SetLabelFont(43)
  yax.SetTitleFont(43)
  
  yax.SetTitle(ytitle)
  yax.SetTitleSize(text_size)
  yax.SetTitleOffset(1.8)    
  
  yax.SetLabelOffset(0.015)
  yax.SetLabelSize(text_size - 7)
 
  ymax = hist.GetMaximum()
  if d_hsig:
      ymax_sig = max(h.GetMaximum() for h in d_hsig.values())
      ymax = max(ymax, ymax_sig)
  if h_dat:
      ymax = max(ymax, h_dat.GetMaximum())
          
  ymin = hist.GetMinimum()
  print("ymax=",ymax)
  # top Tracks panel
  #if xtitle == '':
  if 'Tracks' in ytitle:
    yax.SetNdivisions(505) 
    if IsLogY:
      if enlargeYaxis:
        ymax = 2 * 10 ** 10
        ymin = 0.02
      else:
        ymax = 3 * 10 ** 8
        ymin = 0.09
      hist.SetMaximum(ymax)
      hist.SetMinimum(ymin)
    else:
      hist.SetMaximum(ymax*1.2)
      hist.SetMinimum(0.0)
      
  # bottom data/pred panel 
  elif 'Significance' in ytitle:
    #hist.SetMinimum(0.001)
    #hist.SetMaximum(8.) #max 
    yax.SetNdivisions(205)

  elif 'Data' in ytitle:
    hist.SetMinimum(0.0)
    hist.SetMaximum(2.0) 
    yax.SetNdivisions(205)
   
  gPad.SetTicky()
  gPad.Update()


#____________________________________________________________________________
def myText(x, y, text, tsize=0.05, color=kBlack, angle=0) :
  
  l = TLatex()
  l.SetTextSize(tsize)
  l.SetNDC()
  l.SetTextColor(color)
  l.SetTextAngle(angle)
  l.DrawLatex(x,y,'#bf{' + text + '}')
  l.SetTextFont(4)

#____________________________________________________________________________
def cut_arrow(x1, y1, x2, y2, direction='right', ar_size=1.0, ar_width=10, color=kGray+3, style=1) :
  
  l = TLine(x1, y1, x2, y2)
  if direction == 'right':
    ar = TArrow(x1-0.02, y2, x1+ar_width, y2, ar_size, '|>')
  if direction == 'left':
    ar = TArrow(x1-ar_width+0.02, y2, x1, y2, ar_size, '<|')
  if direction == 'up':
    ar = TArrow(x1, y1, x1, y2, ar_size, '|>')
  if direction == 'down':
    ar = TArrow(x1, y1, x1, y2, ar_size, '<|')
  l.SetLineWidth(4)
  l.SetLineStyle(style)
  l.SetLineColor(color) 
  ar.SetLineWidth(4)
  ar.SetLineStyle(style)
  ar.SetLineColor(color) 
  ar.SetFillColor(color)  
  return [l, ar]

#____________________________________________________________________________
def mk_leg(xmin, ymin, xmax, ymax, region, l_samp, d_samp, nTotBkg, d_hists, d_yield, d_yieldErr, sampSet_type='bkg', txt_size=0.05) :
  '''
  @l_samp : Constructs legend based on list of samples 
  @nTotBkg : Total background Tracks
  @d_hists : The dictionary of histograms 
  @d_samp : May from samples to legend text
  @d_yields : The dictionary of yields 
  @d_yieldErr : Dictionary of errors on the yields
  @sampSet_type : The type of samples in the set of samples in the list 
  '''  

  # ---------------------------------------------------- 
  # Legend: construct and format
  # ---------------------------------------------------- 
  leg = TLegend(xmin,ymin,xmax,ymax)
  leg.SetBorderSize(0)
  leg.SetTextSize(txt_size)
  leg.SetNColumns(1)

  # legend markers 
  d_legMk = {
    'bkg'  : 'f',
    'sig'  : 'l',
    'data' : 'ep'
    }

  # Need to reverse background order so legend is filled as histogram is stacked
  if sampSet_type == 'bkg':
    l_samp = [x for x in reversed(l_samp)]
  for samp in l_samp: 
    #print( 'Processing {0}'.format(samp) )
    # obtain sample attributes 
    hist        = d_hists[samp][0]
    sample_type = d_samp[samp]['type']
    leg_entry   = d_samp[samp]['leg']
    legMk       = d_legMk[sample_type]
   
    #print('samp: {0}, type: {1}, legMk: {2}'.format(samp, sample_type, legMk) ) 
    # calculate the % of each background component and put in legend
    pc_yield   = 0
    if sample_type == 'bkg':
      try: pc_yield = 100 * ( d_yield[samp] / float(nTotBkg) )
      except ZeroDivisionError:  pass
      leg_txt = '{0} ({1:.1f}%)'.format( leg_entry, pc_yield )
      print('{0}, {1}, {2:.3f} +/- {3:.3f}'.format(region, samp, d_yield[samp], d_yieldErr[samp]) )
    elif sample_type == 'sig':
      leg_txt = '{0} ({1:.1f} Tracks)'.format(leg_entry, d_yield[samp])
      print('==============================================')
      print('{0}, {1}, {2:.3f} +/- {3:.3f}'.format(region, samp, d_yield[samp], d_yieldErr[samp]) )
    elif sample_type == 'data':
      leg_txt = '{0} ({1:.0f} Tracks)'.format(leg_entry, d_yield['data'])  
    leg.AddEntry(hist, leg_txt, legMk)
  
  return leg

#____________________________________________________________________________
def draw_line(xmin, ymin, xmax, ymax, color=kGray+1, style=2) :
  
  # draw line of kinematically forbidden region
  line = TLine(xmin , ymin , xmax, ymax)
  line.SetLineWidth(2)
  line.SetLineStyle(style)
  line.SetLineColor(color) # 12 = gray
  return line
 
#_________________________________________________________________________
def mkdir(dirPath):
  '''
  make directory for given input path
  '''
  try:
    os.makedirs(os.path.expandvars(dirPath))
    print ('Successfully made new directory ' + dirPath)
  except OSError:
    pass
 
def ZValue(totalSig,totalBkg,bkguncert):
    """The new recommended Z formula (rather than BinomialExpZ)
    Found at:
    https://cds.cern.ch/record/2643488/files/ATL-COM-GEN-2018-026.pdf?version=4
    """
    nbObs = totalSig+totalBkg #observed tracks
    nbExp = totalBkg 
    nbExpEr = bkguncert*totalBkg

    if nbObs == 0 or nbExp ==0:
        return -1

    factor1 = nbObs*np.log( (nbObs*(nbExp+nbExpEr**2))/(nbExp**2+nbObs*nbExpEr**2) )
    factor2 = (nbExp**2/nbExpEr**2)*np.log( 1 + (nbExpEr**2*(nbObs-nbExp))/(nbExp*(nbExp+nbExpEr**2)) )

    if nbObs < nbExp:
        pull  = -np.sqrt(2*abs(factor1 - factor2))
    else:
        pull  = np.sqrt(2*abs(factor1 - factor2))
    return pull


if __name__ == "__main__":
  #main(sys.argv)
  main()


