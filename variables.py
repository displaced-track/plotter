#____________________________________________________________________________
def configure_vars(sig_reg):

  # format for each variable is
  #'var_name_in_ntuple':{TLaTeX axis entry, units, Nbins, xmin, xmax, arrow position (for N-1 plots), arrow direction}
  # to do variable bin widths, place 'var' as value of 'hXNbins' and specify lower bin edges as 'binsLowE':[0,4,5,11,15,20,40,60]
  # e.g.     'lep2Pt':{'tlatex':'p_{T}(#font[12]{l}_{2})','units':'GeV','hXNbins':'var','hXmin':0,'hXmax':60,'binsLowE':[0,4,5,11,15,20,40,60],'cut_pos':200,'cut_dir':'left'}, 
  d_vars = {

    #---------------------------------
    #my saved variables
    'MET'    :{'tlatex':'E_{T}^{miss}','units':'GeV','hXNbins':16,'hXmin':200.,'hXmax':1000.,'cut_pos':230,'cut_dir':'right'},
    'DNN'    :{'tlatex':'DNN output score','units':'','hXNbins':100,'hXmin':0.,'hXmax':1.,'cut_pos':230,'cut_dir':'right'},
    'trackD0Sig'    :{'tlatex':'track S(d_{0})','units':'','hXNbins':30,'hXmin':0.,'hXmax':30.,'cut_pos':230,'cut_dir':'right'},
  }
  
  return d_vars
