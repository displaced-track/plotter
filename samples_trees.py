
#____________________________________________________________________________
def configure_samples(isData15_16, isData17, isData18, isData15_18, isRJProcessed):
  from ROOT import TColor
  from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange

  # Blues
  myLighterBlue=kAzure-2
  myLightBlue  =TColor.GetColor('#9ecae1')
  myMediumBlue =TColor.GetColor('#0868ac')
  myDarkBlue   =TColor.GetColor('#08306b')

  # Greens
  myLightGreen   =TColor.GetColor('#c7e9c0')
  myMediumGreen  =TColor.GetColor('#41ab5d')
  myDarkGreen    =TColor.GetColor('#006d2c')

  # Oranges
  myLighterOrange=TColor.GetColor('#ffeda0')
  myLightOrange  =TColor.GetColor('#fec49f')
  myMediumOrange =TColor.GetColor('#fe9929')

  # Greys
  myLightestGrey=TColor.GetColor('#f0f0f0')
  myLighterGrey=TColor.GetColor('#e3e3e3')
  myLightGrey  =TColor.GetColor('#969696')

  # Pinks
  myLightPink = TColor.GetColor('#fde0dd')
  myMediumPink = TColor.GetColor('#fcc5c0')
  myDarkPink = TColor.GetColor('#dd3497')

  # Purples
  myLightPurple   = TColor.GetColor('#dadaeb')
  myMediumPurple  = TColor.GetColor('#9e9ac8')
  myDarkPurple    = TColor.GetColor('#6a51a3')
  #myLightPurple   = TColor.GetColor('#967BB6')
  #myMediumPurple  = TColor.GetColor('#66369D')
  #myDarkPurple    = TColor.GetColor('#522888')

  # Turquoise
  myLightTurquoise  = TColor.GetColor('#7FB1B2')
  myMediumTurquoise = TColor.GetColor('#539798')
  myDarkTurquoise   = TColor.GetColor('#297D7D')

  # Reds
  myMediumRed = TColor.GetColor('#EE220D')
  myLightRed = TColor.GetColor('#fc5a49')

  d_samp = {
    'data'     :{'type':'data', 'leg':'data',    'f_color':kBlack,         'l_color':0,  'path' :'data.root'},
    'Zee'      :{'type':'bkg',  'leg':'Zee',     'f_color':myLightBlue,    'l_color':0,  'path' :'Zee.root'},
    'Zmumu'    :{'type':'bkg',  'leg':'Zmumu',   'f_color':myMediumBlue,   'l_color':0,  'path' :'Zmumu.root'},
    'Ztautau'  :{'type':'bkg',  'leg':'Ztautau', 'f_color':myDarkBlue,     'l_color':0,  'path' :'Ztautau.root'},
    'Znunu'    :{'type':'bkg',  'leg':'Znunu',   'f_color':myDarkTurquoise,'l_color':0,  'path' :'Znunu.root'},
    'Wenu'     :{'type':'bkg',  'leg':'Wenu',    'f_color':myLightGreen,   'l_color':0,  'path' :'Wenu.root'},
    'Wmunu'    :{'type':'bkg',  'leg':'Wmunu',   'f_color':myMediumGreen,  'l_color':0,  'path' :'Wmunu.root'},    
    'Wtaunu'   :{'type':'bkg',  'leg':'Wtaunu',  'f_color':myDarkGreen,    'l_color':0,  'path' :'Wtaunu.root'},
    'ttbar'    :{'type':'bkg',  'leg':'t#bar{t}','f_color':myMediumRed,    'l_color':0,  'path' :'ttbar.root'},
    'singletop':{'type':'bkg',  'leg':'Wt',      'f_color':myLightRed,     'l_color':0,  'path' :'singletop.root'},
    'diboson'  :{'type':'bkg',  'leg':'VV',      'f_color':myMediumOrange, 'l_color':0,  'path' :'diboson.root'},
    'dijet'    :{'type':'bkg',  'leg':'dijet',   'f_color':myLightOrange,  'l_color':0,  'path' :'dijet.root'},
                  
    # Signals
    "176_175p5_175" :{"type":"sig","leg":"(176, 175.5, 175)","l_color":  kGreen,"path":"higgsino_176_175.5_175.root"},
    #"151_150p5_150" :{"type":"sig","leg":"(151, 150.5, 150)","l_color":  kGreen,"path":"higgsino_151_150p5_150.root"},
    #"150p7_150p35_150" :{"type":"sig","leg":"(150.7, 150.35, 150)","l_color":  myMediumBlue,"path":"higgsino_150p7_150p35_150.root"},
    #"150p5_150p5_150" :{"type":"sig","leg":"(150.5, 150.5, 150)","l_color":  myDarkGreen,"path":"higgsino_150p5_150p5_150.root"},
    #"151_151_150" :{"type":"sig","leg":"(151, 151, 150)","l_color":  myLightPurple,"path":"higgsino_151_151_150.root"},
    }

  return d_samp


